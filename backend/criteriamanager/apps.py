from django.apps import AppConfig


class CriteriamanagerConfig(AppConfig):
    name = 'criteriamanager'
