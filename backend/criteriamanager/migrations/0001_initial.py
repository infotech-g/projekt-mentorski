# Generated by Django 3.0 on 2021-01-26 19:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LearningEffect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Profession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Qualification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('profession', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Profession')),
            ],
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('year', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('userName', models.CharField(max_length=20)),
                ('hashedPassword', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='VerificationCriterion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('learningEffect', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.LearningEffect')),
            ],
        ),
        migrations.CreateModel(
            name='Subqualification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('qualification', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Qualification')),
            ],
        ),
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lessonNumber', models.PositiveIntegerField()),
                ('topic', models.CharField(max_length=100)),
                ('section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Section')),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Subject')),
            ],
        ),
        migrations.AddField(
            model_name='learningeffect',
            name='subqualification',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Subqualification'),
        ),
        migrations.CreateModel(
            name='Asignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lesson', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.Lesson')),
                ('verificationCriterion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='criteriamanager.VerificationCriterion')),
            ],
        ),
    ]
