from django.shortcuts import render

# Create your views here.

from rest_framework import generics
from .models import *
from .serializers import *


class SubjectListView(generics.ListCreateAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SubjectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class SectionListView(generics.ListCreateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer


class SectionDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer


class LessonListView(generics.ListCreateAPIView):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer


class LessonDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer


class ProfessionListView(generics.ListCreateAPIView):
    queryset = Profession.objects.all()
    serializer_class = ProfessionSerializer


class ProfessionDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Profession.objects.all()
    serializer_class = ProfessionSerializer


class QualificationListView(generics.ListCreateAPIView):
    queryset = Qualification.objects.all()
    serializer_class = QualificationSerializer


class QualificationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Qualification.objects.all()
    serializer_class = QualificationSerializer


class SubqualificationListView(generics.ListCreateAPIView):
    queryset = Subqualification.objects.all()
    serializer_class = SubqualificationSerializer


class SubqualificationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Subqualification.objects.all()
    serializer_class = SubqualificationSerializer


class LearningEffectListView(generics.ListCreateAPIView):
    queryset = LearningEffect.objects.all()
    serializer_class = LearningEffectSerializer


class LearningEffectDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = LearningEffect.objects.all()
    serializer_class = LearningEffectSerializer


class VerificationCriterionListView(generics.ListCreateAPIView):
    queryset = VerificationCriterion.objects.all()
    serializer_class = VerificationCriterionSerializer


class VerificationCriterionDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = VerificationCriterion.objects.all()
    serializer_class = VerificationCriterionSerializer


class AsignmentListView(generics.ListCreateAPIView):
    queryset = Asignment.objects.all()
    serializer_class = AsignmentSerializer


class AsignmentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Asignment.objects.all()
    serializer_class = AsignmentSerializer
