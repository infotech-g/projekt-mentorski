import { SubqualificationService } from '../../services/subqualification.service';
import { Subqualification } from '../../models/subqualification';
import { QualificationService } from '../../services/qualification.service';
import { Qualification } from '../../models/qualification';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qualifications',
  templateUrl: './qualifications.component.html',
  styleUrls: ['./qualifications.component.css']
})
export class QualificationsComponent implements OnInit {

  /**
   * q - qualifications
   * sq - subqualifications
   */

  q: Qualification[];
  sq: Subqualification[];
  selectedQ: Qualification;
  selectedSq: Subqualification;
  assignedSq: Subqualification[];
  
  constructor(
    private QualificationService: QualificationService,
    private SubqualificationService: SubqualificationService
    ){ 
  }

  ngOnInit() {
    this.getSq();
    this.getQ();
  }

  getSq(): void {
    this.SubqualificationService.getAll().subscribe(
      (subqualifications: Subqualification[]) => this.sq = subqualifications)
  }

  getQ(): void {
    this.QualificationService.getAll().subscribe(
      (qualifications: Qualification[]) => {
        this.q = qualifications
        this.onSelectQ(this.q[0])
      }
    );  
  }

  onSelectQ(qualification: Qualification): void {
    this.selectedQ = qualification;
    this.getSqOf(qualification.id)
  }

  onSelectSq(subqualification): void {
    this.selectedSq = subqualification;
  }
  
  getSqOf(qualificationId: number): void {
    this.assignedSq = [];
    for (let i=0; i < this.sq.length; i++) {
      if (this.sq[i].qualification === qualificationId) {
        this.assignedSq.push(this.sq[i])
      }
    }
  }
}
