export interface Qualification {
    id: number;
    name: string;
    profession: string;
}