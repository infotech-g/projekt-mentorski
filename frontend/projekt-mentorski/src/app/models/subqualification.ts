export interface Subqualification {
    id: number;
    name: string;
    qualification: number;
}