import { Observable, of } from 'rxjs';
import { Qualification } from '../models/qualification';
import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';

const baseUrl = 'http://localhost:8000/api/qualifications/?format=json';

@Injectable({
  providedIn: 'root'
})
export class QualificationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Qualification[]> {
    return this.http.get<Qualification[]>(baseUrl);
  }
}
