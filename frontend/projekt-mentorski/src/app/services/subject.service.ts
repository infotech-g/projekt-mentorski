import { Subject } from '../models/subject';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from  '@angular/common/http';

const baseUrl = 'http://localhost:8000/api/subjects/?format=json';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  
  constructor(private http: HttpClient) { }

  getAll(): Observable<Subject[]> {
    return this.http.get<Subject[]>(baseUrl);
  }
}
