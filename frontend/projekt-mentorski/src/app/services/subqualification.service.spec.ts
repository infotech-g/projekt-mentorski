import { TestBed } from '@angular/core/testing';

import { SubqualificationService } from './subqualification.service';

describe('SubqualificationService', () => {
  let service: SubqualificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubqualificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
