import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Subqualification } from 'app/models/subqualification';

const baseUrl = 'http://localhost:8000/api/subqualifications/?format=json';

@Injectable({
  providedIn: 'root'
})
export class SubqualificationService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Subqualification[]> {
    return this.http.get<Subqualification[]>(baseUrl);
  }
}
